@extends('templates.backend.master')
@section('content')
    <div class="page-header">
        <ol class="breadcrumb">
            <li><a href="">Trang chủ</a></li>
            <li><a href="{{ route('admin.article.create') }}">Bài viết</a></li>
            <li class="active">Thêm mới</li>
        </ol>
    </div>
    <div class="">
        @include('backend.articles.form')
    </div>
@stop