<div class="col-sm-8">
<form action="" method="post" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label for="email">Loại bài viết:</label>
        <select name="a_category_id" id="" class="form-control">
            <option value="">--Chọn loại bài viết--</option>
            <?php cate_parent($categories,0,"--",old('a_category_id',isset($article->a_category_id) ? $article->a_category_id : '')); ?>
        </select>
        @if($errors->has('a_category_id'))
            <div class="error-text">
                {{$errors->first('a_category_id')}}
            </div>
        @endif
    </div>

    <div class="form-group">
        <label for="a_name">Tên bài viết:</label>
        <input type="text" class="form-control"  placeholder="Tên bài viết" value="{{ old('a_name',isset($article->a_name) ? $article->a_name : '') }}" name="a_name">
        @if($errors->has('a_name'))
            <div class="error-text">
                {{$errors->first('a_name')}}
            </div>
        @endif
    </div>

    <div class="form-group">
        <label for="a_description">Mô tả:</label>
        <textarea name="a_description" class="form-control" id="" cols="30" rows="3" placeholder="Mô tả ngắn bài viết">{{ old('a_description',isset($article->a_description) ? $article->a_description : '') }}</textarea>
        @if($errors->has('a_description'))
            <div class="error-text">
                {{$errors->first('a_description')}}
            </div>
        @endif
    </div>

    <div class="form-group">
        <label for="pro_content">Nội dung:</label>
        <textarea name="a_content" class="form-control" id="pro_content" cols="30" rows="3" placeholder="Nội dung bài viết">{{ old('a_content',isset($article->a_content) ? $article->a_content : '') }}</textarea>
        @if($errors->has('a_content'))
            <div class="error-text">
                {{$errors->first('a_content')}}
            </div>
        @endif
    </div>

    <div class="form-group">
        @if(isset($article->a_avatar))
        <p><img src="{{ pare_url_file($article->a_avatar) }}" alt="" style="width: 80px;"><img src="{{getenv('admin_url')}}/images/default-image.jpg" id="out_img" alt="" width="80px" height="80px"></p>
        @endif
    </div>

    <div class="form-group">
        <label for="avatar">Avatar:</label>
        <input type="file" name="avatar" class="form-control">
    </div>


    <div class="form-group">
        <label for="avatar">Tag:</label>
        <input type="text" value="{{ old('a_tag',isset($article->a_tag) ? $article->a_tag : '') }}" name="a_tag" data-role="tagsinput" class="form-control">
    </div>


    <div class="form-group">
        <label for="a_title_seo">Meta title:</label>
        <input type="text" class="form-control"  placeholder="Meta Title" value="{{ old('a_title_seo',isset($article->a_title_seo) ? $article->a_title_seo : '') }}" name="a_title_seo">
    </div>

    <div class="form-group">
        <label for="a_description_seo">Meta Description:</label>
        <input type="text" class="form-control"  placeholder="Meta Description" value="{{ old('a_description_seo',isset($article->a_description_seo) ? $article->a_description_seo : '') }}" name="a_description_seo">
    </div>

    <div class="form-group">
        <div class="checkbox">
            <label><input type="checkbox" name="pro_hot">Nổi bật</label>
        </div>
    </div>
    <button type="submit" class="btn btn-success">Lưu thông tin</button>
</form>
</div>
@section('script')
    <script src="{{getenv('admin_url')}}/ckeditor/ckeditor.js"></script>
    <script>
        CKEDITOR.replace('pro_content');
    </script>

@stop