<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Frontend
Route::group(['namespace' =>'FrontEnd'],function (){
    Route::get('/','HomeController@index')->name('home');

});

//backend
Route::group(['namespace' =>'Backend','prefix'=>'admin'],function (){
    //dashboard
    Route::get('/','DashboardController@index')->name('admin.dashboard');

    //category
    Route::group(['prefix'=>'danh-muc'],function(){
        Route::get('/','CategoryController@index')->name('admin.category.index');
        Route::get('/create','CategoryController@create')->name('admin.category.create');
        Route::post('/create','CategoryController@createPost');
        Route::get('/update/{id}','CategoryController@update')->name('admin.category.update');
        Route::post('/update/{id}','CategoryController@updatePost');
        Route::get('/{action}/{id}','CategoryController@action')->name('admin.category.action');
    });

    //baiviet
    Route::group(['prefix'=>'bai-viet'],function(){
        Route::get('/','ArticleController@index')->name('admin.article.index');
        Route::get('/create','ArticleController@create')->name('admin.article.create');
        Route::post('/create','ArticleController@createPost');
        Route::get('/update/{id}','ArticleController@update')->name('admin.article.update');
        Route::post('/update/{id}','ArticleController@updatePost');
        Route::get('/{action}/{id}','ArticleController@action')->name('admin.article.action');
    });

});


