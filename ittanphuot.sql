-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 09, 2019 at 05:12 PM
-- Server version: 10.3.15-MariaDB
-- PHP Version: 7.1.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ittanphuot`
--

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE `articles` (
  `id` int(10) UNSIGNED NOT NULL,
  `a_category_id` int(11) NOT NULL DEFAULT 0,
  `a_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `a_slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `a_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `a_content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `a_active` tinyint(4) NOT NULL DEFAULT 1,
  `a_hot` tinyint(4) NOT NULL DEFAULT 0,
  `a_author_id` int(11) NOT NULL DEFAULT 0,
  `a_description_seo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `a_title_seo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `a_avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `a_view` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `a_tag` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`id`, `a_category_id`, `a_name`, `a_slug`, `a_description`, `a_content`, `a_active`, `a_hot`, `a_author_id`, `a_description_seo`, `a_title_seo`, `a_avatar`, `a_view`, `created_at`, `updated_at`, `a_tag`) VALUES
(1, 1, 'ddasd', 'ddasd', 'dasdsa', '<p>\r\n	dsadsa</p>', 1, 0, 0, 'dsad', 'dsad', NULL, 0, '2019-12-06 01:53:37', '2019-12-06 02:11:56', ''),
(3, 1, 'ddddddddddddddddddd', 'ddddddddddddddddddd', 'ddddddddddddddddđ', '<p>\r\n	ddddddddddddddd</p>', 1, 0, 0, 'ddddddddddddddddđ', 'ddddddddddddddddddd', NULL, 0, '2019-12-06 01:55:08', '2019-12-06 01:55:08', ''),
(4, 3, 'ffffffffffadadsadaaaaaaxxxxx', 'ffffffffffadadsadaaaaaaxxxxx', 'ffffffffffadadsadaaaaaaxxxxxxx', '<p>\r\n	ffffffffffadadsadaaaaaaxxxx</p>', 1, 0, 0, 'ffffffffffadadsadaaaaaaxxx', 'ffffffffffadadsadaaaaaaxx', '2019-12-06__e1a6ea3fab994dc71488.jpg', 0, '2019-12-06 02:19:26', '2019-12-06 05:32:58', ''),
(5, 5, 'Khác biệt giữa Inline, External và Internal style CSS', 'khac-biet-giua-inline-external-va-internal-style-css', 'Có 3 cách để thêm style CSS vào website của bạn: bạn có thể dùng internal CSS và chèn CSS rules trong mục <head> trong HTML document, liên kết nó với file external .css chứa CSS rules hoặc sử dụng inline CSS để áp rules vào elements được chỉ định. Bài hướng dẫn này sẽ đi qua cả 3 cách, ưu điểm và nhược điểm của chúng.', '<h2 style=\"box-sizing: border-box; font-size: 2.8rem; line-height: 4.2rem; margin: 0px 0px 1.8rem; font-family: &quot;Open Sans&quot;; color: rgb(51, 51, 51);\">\r\n	<span id=\"Gioi-thieu\" style=\"box-sizing: border-box;\">Giới thiệu</span></h2>\r\n<p>\r\n	<span style=\"color: rgb(68, 68, 68); font-family: &quot;Open Sans&quot;; font-size: 16px;\">C&oacute; 3 c&aacute;ch để th&ecirc;m style CSS&nbsp;v&agrave;o website của bạn: bạn c&oacute; thể d&ugrave;ng internal CSS v&agrave; ch&egrave;n CSS rules trong mục &lt;head&gt;&nbsp;trong HTML document, li&ecirc;n kết n&oacute; với file external&nbsp;</span><span style=\"box-sizing: border-box; font-weight: 700; color: rgb(68, 68, 68); font-family: &quot;Open Sans&quot;; font-size: 16px;\">.css&nbsp;</span><span style=\"color: rgb(68, 68, 68); font-family: &quot;Open Sans&quot;; font-size: 16px;\">chứa&nbsp;CSS rules hoặc sử dụng inline CSS để &aacute;p rules v&agrave;o elements được chỉ định. B&agrave;i hướng dẫn n&agrave;y sẽ đi qua cả 3 c&aacute;ch, ưu điểm v&agrave; nhược điểm của ch&uacute;ng.</span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<h2 style=\"box-sizing: border-box; font-size: 2.8rem; line-height: 4.2rem; margin: 0px 0px 1.8rem; font-family: &quot;Open Sans&quot;; color: rgb(51, 51, 51);\">\r\n	<span id=\"Phan-1-8211-Internal-CSS\" style=\"box-sizing: border-box;\">Phần 1 &ndash; Internal CSS</span></h2>\r\n<p>\r\n	<span style=\"color: rgb(68, 68, 68); font-family: &quot;Open Sans&quot;; font-size: 16px;\">Internal CSS code được đặt trong mục &lt;head&gt;&nbsp;của một trang nhất định. Classes v&agrave; IDs c&oacute; thể được chỉ định tới CSS, nhưng n&oacute; chỉ hoạt động tr&ecirc;n trang c&oacute; định đ&oacute;. &nbsp;Classes v&agrave; IDs c&oacute; thể được d&ugrave;ng để dẫn tới code CSS, nhưng chỉ hoạt động tr&ecirc;n trang đ&oacute;. Style CSS&nbsp;được nh&uacute;ng bằng c&aacute;ch n&agrave;y sẽ cần phải được tải về mỗi khi trang được load, v&igrave; vậy c&oacute; thể dẫn đến mất nhiều thời gian để duyệt website. Tuy nhi&ecirc;n, trong một số trường hợp internal stylessheet rất hữu dụng. Một v&iacute; dụ l&agrave; bạn muốn gửi một trang mẫu &ndash; v&igrave; tất cả đều chỉ nằm trong một trang &ndash; xem preview sẽ dễ hơn nhiều. Internal CSS được đặt trong thẻ &lt;style&gt;&lt;/style&gt;. Một v&iacute; dụ của internal stylesheet:</span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<pre>\r\n&lt;head&gt;\r\n &nbsp;&lt;style type=&quot;text/css&quot;&gt;\r\n &nbsp; &nbsp;p {color:white; font-size: 10px;}\r\n &nbsp; &nbsp;.center {display: block; margin: 0 auto;}\r\n &nbsp; &nbsp;#button-go, #button-back {border: solid 1px black;}\r\n &nbsp;&lt;/style&gt;\r\n&lt;/head&gt;</pre>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 2rem; color: rgb(68, 68, 68); font-family: &quot;Open Sans&quot;; font-size: 16px;\">\r\n	<span style=\"box-sizing: border-box; font-weight: 700;\">&Iacute;ch lợi của</span><span style=\"box-sizing: border-box; font-weight: 700;\">&nbsp;Internal CSS:</span></p>\r\n<ul style=\"box-sizing: border-box; margin: 0px 0px 2rem; padding-left: 20px; color: rgb(68, 68, 68); font-family: &quot;Open Sans&quot;; font-size: 16px;\">\r\n	<li style=\"box-sizing: border-box; margin-bottom: 1rem;\">\r\n		Chỉ một trang ảnh hưởng bởi&nbsp;stylesheet.</li>\r\n	<li style=\"box-sizing: border-box; margin-bottom: 1rem;\">\r\n		Classes v&agrave; IDs c&oacute; thể được d&ugrave;ng bởi internal stylessheet.</li>\r\n	<li style=\"box-sizing: border-box; margin-bottom: 1rem;\">\r\n		Kh&ocirc;ng cần phải upload nhiều files. HTML v&agrave; CSS c&oacute; thể l&agrave; một file.</li>\r\n</ul>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 2rem; color: rgb(68, 68, 68); font-family: &quot;Open Sans&quot;; font-size: 16px;\">\r\n	<span style=\"box-sizing: border-box; font-weight: 700;\">Bất lợi của Internal CSS:</span></p>\r\n<ul style=\"box-sizing: border-box; margin: 0px 0px 2rem; padding-left: 20px; color: rgb(68, 68, 68); font-family: &quot;Open Sans&quot;; font-size: 16px;\">\r\n	<li style=\"box-sizing: border-box; margin-bottom: 1rem;\">\r\n		Tăng thời gian load time.</li>\r\n	<li style=\"box-sizing: border-box; margin-bottom: 1rem;\">\r\n		N&oacute; ảnh hưởng tới một trang &ndash; kh&ocirc;ng hữu dụng nếu bạn sử dụng c&ugrave;ng một CSS cho nhiều trang..</li>\r\n</ul>\r\n<h3 style=\"box-sizing: border-box; font-size: 2.3rem; line-height: 3rem; margin: 0px 0px 1.4rem; font-family: &quot;Open Sans&quot;; color: rgb(51, 51, 51);\">\r\n	<span style=\"box-sizing: border-box;\">L&agrave;m thế n&agrave;o để th&ecirc;m Internal CSS v&agrave;o trang HTML</span></h3>\r\n<ol style=\"box-sizing: border-box; margin: 0px 0px 3rem; padding-left: 24px; color: rgb(68, 68, 68); font-family: &quot;Open Sans&quot;; font-size: 16px;\">\r\n	<li style=\"box-sizing: border-box; margin-bottom: 1rem;\">\r\n		Mở trang HTML với một text editor. Nếu trang đ&atilde; được upload l&ecirc;n t&agrave;i khoản hosting, bạn c&oacute; thể sử dụng text editor được cung cấp sẳn của hositng. Nếu bạn c&oacute; một t&agrave;i liệu HTML tr&ecirc;n m&aacute;y t&iacute;nh của bạn, bạn c&oacute; thể sử dụng text editors để sửa n&oacute; v&agrave; upload lại files đ&oacute; v&agrave;o t&agrave;i khoản hosting của bạn qua&nbsp;<a data-wpel-link=\"internal\" href=\"https://www.hostinger.com/tutorials/ftp/filezilla-ftp-configuration\" rel=\"noopener noreferrer\" style=\"box-sizing: border-box; text-decoration-line: none; background-color: transparent; color: rgb(103, 71, 199); cursor: pointer; transition: border-bottom 0.15s ease-in 0s; padding-bottom: 2px; border-bottom: 2px solid transparent;\" target=\"_blank\">FTP client</a>.</li>\r\n	<li style=\"box-sizing: border-box; margin-bottom: 1rem;\">\r\n		Định vị tag &lt;head&gt;&nbsp;mở đầu v&agrave; th&ecirc;m d&ograve;ng sau v&agrave;o n&oacute;:</li>\r\n</ol>\r\n<pre style=\"box-sizing: border-box; font-family: monospace, monospace; font-size: 15px; border-radius: 3px; padding: 10px 20px; margin-bottom: 2rem; background: rgba(0, 0, 0, 0.1); border: 1px solid transparent; overflow-wrap: normal; color: rgb(68, 68, 68); overflow: auto !important;\">\r\n&lt;style type=&quot;text/css&quot;&gt;</pre>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 2rem; color: rgb(68, 68, 68); font-family: &quot;Open Sans&quot;; font-size: 16px;\">\r\n	3. B&acirc;y giờ tới d&ograve;ng mới v&agrave; th&ecirc;m v&agrave;o CSS rules, v&iacute; dụ:</p>\r\n<pre style=\"box-sizing: border-box; font-family: monospace, monospace; font-size: 15px; border-radius: 3px; padding: 10px 20px; margin-bottom: 2rem; background: rgba(0, 0, 0, 0.1); border: 1px solid transparent; overflow-wrap: normal; color: rgb(68, 68, 68); overflow: auto !important;\">\r\nbody {\r\n &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; background-color: blue;\r\n &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}\r\n h1 {\r\n &nbsp; &nbsp; color: red;\r\n &nbsp; &nbsp; padding: 60px;\r\n }</pre>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 2rem; color: rgb(68, 68, 68); font-family: &quot;Open Sans&quot;; font-size: 16px;\">\r\n	4. Khi đ&atilde; th&ecirc;m CSS rules, h&atilde;y đ&oacute;ng tag bằng:</p>\r\n<pre style=\"box-sizing: border-box; font-family: monospace, monospace; font-size: 15px; border-radius: 3px; padding: 10px 20px; margin-bottom: 2rem; background: rgba(0, 0, 0, 0.1); border: 1px solid transparent; overflow-wrap: normal; color: rgb(68, 68, 68); overflow: auto !important;\">\r\n&lt;/style&gt;</pre>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 2rem; color: rgb(68, 68, 68); font-family: &quot;Open Sans&quot;; font-size: 16px;\">\r\n	<span style=\"box-sizing: border-box; font-weight: 700;\">Vậy l&agrave;, HTML file với internal stylesheet sẽ c&oacute; dạng</span><span style=\"box-sizing: border-box; font-weight: 700;\">:</span></p>\r\n<pre style=\"box-sizing: border-box; font-family: monospace, monospace; font-size: 15px; border-radius: 3px; padding: 10px 20px; margin-bottom: 2rem; background: rgba(0, 0, 0, 0.1); border: 1px solid transparent; overflow-wrap: normal; color: rgb(68, 68, 68); overflow: auto !important;\">\r\n<u style=\"box-sizing: border-box;\">&lt;!DOCTYPE html&gt;</u>\r\n&lt;html&gt;\r\n&lt;head&gt;\r\n&lt;style&gt;\r\nbody&nbsp;{\r\n &nbsp; &nbsp;background-color:&nbsp;<u style=\"box-sizing: border-box;\">blue</u>;\r\n}\r\nh1&nbsp;{\r\n &nbsp; &nbsp;color:&nbsp;<u style=\"box-sizing: border-box;\">red</u>;\r\n &nbsp; &nbsp;padding:&nbsp;<u style=\"box-sizing: border-box;\">60px</u>;\r\n}&nbsp;\r\n&lt;/style&gt;\r\n&lt;/head&gt;\r\n&lt;body&gt;\r\n\r\n&lt;h1&gt;Hostinger Tutorials&lt;/h1&gt;\r\n&lt;p&gt;This is our paragraph.&lt;/p&gt;\r\n\r\n&lt;/body&gt;\r\n&lt;/html&gt;</pre>\r\n<h2 style=\"box-sizing: border-box; font-size: 2.8rem; line-height: 4.2rem; margin: 0px 0px 1.8rem; font-family: &quot;Open Sans&quot;; color: rgb(51, 51, 51);\">\r\n	<span id=\"Lua-chon-2-8211-External-CSS\" style=\"box-sizing: border-box;\"><span style=\"box-sizing: border-box;\">Lựa chọn&nbsp;</span><span style=\"box-sizing: border-box;\">2 &ndash; External CSS</span></span></h2>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 2rem; color: rgb(68, 68, 68); font-family: &quot;Open Sans&quot;; font-size: 16px;\">\r\n	C&oacute; lẽ l&agrave; c&aacute;ch dễ nhất để th&ecirc;m Style CSS v&agrave;o website của bạn l&agrave; li&ecirc;n kết với một file .css ở b&ecirc;n ngo&agrave;i. Bằng c&aacute;ch n&agrave;y bất kỳ thay đổi n&agrave;o bạn thực hiện tr&ecirc;n CSS sẽ thay đổi tr&ecirc;n to&agrave;n website. Một c&aacute;ch để l&agrave;m l&agrave; đặt link CSS trong mục &lt;head&gt;&nbsp;của trang:</p>\r\n<pre style=\"box-sizing: border-box; font-family: monospace, monospace; font-size: 15px; border-radius: 3px; padding: 10px 20px; margin-bottom: 2rem; background: rgba(0, 0, 0, 0.1); border: 1px solid transparent; overflow-wrap: normal; color: rgb(68, 68, 68); overflow: auto !important;\">\r\n&lt;head&gt;\r\n &nbsp;&lt;link rel=&quot;stylesheet&quot; type=&quot;text/css&quot; href=&quot;style.css&quot; /&gt;\r\n&lt;/head&gt;</pre>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 2rem; color: rgb(68, 68, 68); font-family: &quot;Open Sans&quot;; font-size: 16px;\">\r\n	Trong đ&oacute; &nbsp;<span style=\"box-sizing: border-box; font-weight: 700;\">style.css</span>&nbsp;chứa tất cả c&aacute;c style rules. V&iacute; dụ:</p>\r\n<pre style=\"box-sizing: border-box; font-family: monospace, monospace; font-size: 15px; border-radius: 3px; padding: 10px 20px; margin-bottom: 2rem; background: rgba(0, 0, 0, 0.1); border: 1px solid transparent; overflow-wrap: normal; color: rgb(68, 68, 68); overflow: auto !important;\">\r\n.xleftcol {\r\n &nbsp; float: left;\r\n &nbsp; width: 33%;\r\n &nbsp; background:#809900;\r\n}\r\n.xmiddlecol {\r\n &nbsp; float: left;\r\n &nbsp; width: 34%;\r\n &nbsp; background:#eff2df;\r\n}</pre>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 2rem; color: rgb(68, 68, 68); font-family: &quot;Open Sans&quot;; font-size: 16px;\">\r\n	<span style=\"box-sizing: border-box; font-weight: 700;\">Thuận lợi của&nbsp;</span><span style=\"box-sizing: border-box; font-weight: 700;\">External CSS:</span></p>\r\n<ul style=\"box-sizing: border-box; margin: 0px 0px 2rem; padding-left: 20px; color: rgb(68, 68, 68); font-family: &quot;Open Sans&quot;; font-size: 16px;\">\r\n	<li style=\"box-sizing: border-box; margin-bottom: 1rem;\">\r\n		K&iacute;ch thước trang HTML nhỏ hơn cấu tr&uacute;c gọn hơn.</li>\r\n	<li style=\"box-sizing: border-box; margin-bottom: 1rem;\">\r\n		Tốc độ load trang nhanh hơn.</li>\r\n	<li style=\"box-sizing: border-box; margin-bottom: 1rem;\">\r\n		Một file&nbsp;<span style=\"box-sizing: border-box; font-weight: 700;\">.css</span>&nbsp;c&oacute; thể được d&ugrave;ng cho nhiều.</li>\r\n</ul>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 2rem; color: rgb(68, 68, 68); font-family: &quot;Open Sans&quot;; font-size: 16px;\">\r\n	<span style=\"box-sizing: border-box; font-weight: 700;\">Bất lợi của External CSS:</span></p>\r\n<ul style=\"box-sizing: border-box; margin: 0px 0px 2rem; padding-left: 20px; color: rgb(68, 68, 68); font-family: &quot;Open Sans&quot;; font-size: 16px;\">\r\n	<li style=\"box-sizing: border-box; margin-bottom: 1rem;\">\r\n		Cho tới khi external CSS được load l&ecirc;n, trang của bạn sẽ kh&ocirc;ng được tải ho&agrave;n to&agrave;n.</li>\r\n</ul>\r\n<h2 style=\"box-sizing: border-box; font-size: 2.8rem; line-height: 4.2rem; margin: 0px 0px 1.8rem; font-family: &quot;Open Sans&quot;; color: rgb(51, 51, 51);\">\r\n	<span id=\"Lua-chon-3-8211-Inline-CSS\" style=\"box-sizing: border-box;\">Lựa chọn 3 &ndash; Inline CSS</span></h2>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 2rem; color: rgb(68, 68, 68); font-family: &quot;Open Sans&quot;; font-size: 16px;\">\r\n	Inline CSS được sử dụng cho một thẻ HTML x&aacute;c định. &lt;style&gt;&nbsp;attribute được d&ugrave;ng để style một HTML tag x&aacute;c định. Sử dụng CSS bằng c&aacute;ch n&agrave;y kh&ocirc;ng được khuyến kh&iacute;ch, v&igrave; mỗi tag HTML cần được styles độc lập. Quản l&yacute; website sẽ rất kh&oacute; nếu bạn chỉ sử dụng inline CSS. V&iacute; dụ, trong trường hợp bạn kh&ocirc;ng c&oacute; quyền truy cập tới file CSS hoặc cần &aacute;p dụng style cho một element duy nhất. V&iacute; dụ của một trang HTML với inline CSS sẽ giống như sau:</p>\r\n<pre style=\"box-sizing: border-box; font-family: monospace, monospace; font-size: 15px; border-radius: 3px; padding: 10px 20px; margin-bottom: 2rem; background: rgba(0, 0, 0, 0.1); border: 1px solid transparent; overflow-wrap: normal; color: rgb(68, 68, 68); overflow: auto !important;\">\r\n<u style=\"box-sizing: border-box;\">&lt;!DOCTYPE html&gt;</u>\r\n&lt;html&gt;\r\n&lt;body&nbsp;style=&quot;background-color:black;&quot;&gt;\r\n\r\n&lt;h1&nbsp;style=&quot;color:white;padding:30px;&quot;&gt;Hostinger Tutorials&lt;/h1&gt;\r\n&lt;p&nbsp;style=&quot;color:white;&quot;&gt;Something usefull here.&lt;/p&gt;\r\n\r\n&lt;/body&gt;\r\n&lt;/html&gt;\r\n\r\n</pre>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 2rem; color: rgb(68, 68, 68); font-family: &quot;Open Sans&quot;; font-size: 16px;\">\r\n	<span style=\"box-sizing: border-box; font-weight: 700;\">Thuận lợi của Inline CSS</span><span style=\"box-sizing: border-box; font-weight: 700;\">:</span></p>\r\n<ul style=\"box-sizing: border-box; margin: 0px 0px 2rem; padding-left: 20px; color: rgb(68, 68, 68); font-family: &quot;Open Sans&quot;; font-size: 16px;\">\r\n	<li style=\"box-sizing: border-box; margin-bottom: 1rem;\">\r\n		Hữu dụng nếu bạn muốn test hoặc xem trước thay đổi.</li>\r\n	<li style=\"box-sizing: border-box; margin-bottom: 1rem;\">\r\n		Hữu dụng để sửa nhanh.</li>\r\n	<li style=\"box-sizing: border-box; margin-bottom: 1rem;\">\r\n		Số requests HTTP &iacute;t hơn.</li>\r\n</ul>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 2rem; color: rgb(68, 68, 68); font-family: &quot;Open Sans&quot;; font-size: 16px;\">\r\n	<span style=\"box-sizing: border-box; font-weight: 700;\">Bất lợi của Inline CSS:</span></p>\r\n<ul style=\"box-sizing: border-box; margin: 0px 0px 2rem; padding-left: 20px; color: rgb(68, 68, 68); font-family: &quot;Open Sans&quot;; font-size: 16px;\">\r\n	<li style=\"box-sizing: border-box; margin-bottom: 1rem;\">\r\n		Inline CSS phải được &aacute;p dụng cho mỗi element.</li>\r\n</ul>\r\n<h2 style=\"box-sizing: border-box; font-size: 2.8rem; line-height: 4.2rem; margin: 0px 0px 1.8rem; font-family: &quot;Open Sans&quot;; color: rgb(51, 51, 51);\">\r\n	<span id=\"Ket-luan\" style=\"box-sizing: border-box;\">Kết luận</span></h2>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 2rem; color: rgb(68, 68, 68); font-family: &quot;Open Sans&quot;; font-size: 16px;\">\r\n	B&acirc;y giờ bạn đ&atilde; biết nhiều c&aacute;ch để th&ecirc;m style CSS cho website của bạn v&agrave; biết sự kh&aacute;c biệt giữa inline, external v&agrave; internal stylesheet</p>', 1, 0, 0, 'Có 3 cách để thêm s', 'Khác biệt giữa Inline, External và Internal style CSS', '2019-12-06__2ca9770836aed0f089bf.jpg', 0, '2019-12-06 08:08:34', '2019-12-06 08:09:08', ''),
(6, 6, 'bai viet ve js', 'bai-viet-ve-js', 'bai viet ve js', '<p>\r\n	bai viet ve js</p>', 1, 0, 0, 'bai viet ve js', 'bai viet ve js', NULL, 0, '2019-12-07 03:18:46', '2019-12-07 03:24:56', 'js,html,css,tandeptrai');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `c_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `c_slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `c_icon` char(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `c_avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `c_active` tinyint(4) NOT NULL DEFAULT 0,
  `c_total_product` int(11) NOT NULL DEFAULT 0,
  `c_title_seo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `c_description_seo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `c_keyword_seo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `parent_id` int(11) NOT NULL,
  `c_home` tinyint(4) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `c_name`, `c_slug`, `c_icon`, `c_avatar`, `c_active`, `c_total_product`, `c_title_seo`, `c_description_seo`, `c_keyword_seo`, `created_at`, `updated_at`, `parent_id`, `c_home`) VALUES
(1, 'PHP', 'php', NULL, NULL, 1, 0, 'PHP', NULL, NULL, '2019-12-04 07:42:46', '2019-12-04 08:16:00', 0, 1),
(3, 'Laravel', 'laravel', NULL, NULL, 1, 0, 'Laravel', NULL, NULL, '2019-12-04 08:47:26', '2019-12-04 08:59:47', 1, 1),
(4, 'HTML', 'html', NULL, NULL, 1, 0, 'HTML', NULL, NULL, '2019-12-06 06:37:28', '2019-12-06 06:38:00', 0, 1),
(5, 'CSS', 'css', NULL, NULL, 1, 0, 'CSS', NULL, NULL, '2019-12-06 06:37:35', '2019-12-06 06:38:04', 0, 1),
(6, 'JS', 'js', NULL, NULL, 1, 0, 'JS', NULL, NULL, '2019-12-06 06:37:53', '2019-12-06 06:38:05', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_12_04_134102_create_categories_table', 1),
(4, '2019_12_04_142718_alter_column_parent_id_table_categories', 2),
(5, '2019_12_05_052237_create_articles_table', 3),
(6, '2019_12_07_082637_alter_column_a_tag_table_articles', 4);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `articles_a_name_unique` (`a_name`),
  ADD KEY `articles_a_category_id_index` (`a_category_id`),
  ADD KEY `articles_a_slug_index` (`a_slug`),
  ADD KEY `articles_a_active_index` (`a_active`),
  ADD KEY `articles_a_hot_index` (`a_hot`),
  ADD KEY `articles_a_author_id_index` (`a_author_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_c_name_unique` (`c_name`),
  ADD KEY `categories_c_slug_index` (`c_slug`),
  ADD KEY `categories_c_active_index` (`c_active`),
  ADD KEY `categories_parent_id_index` (`parent_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
