<?php

namespace App\Http\Controllers\Backend;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;

class CategoryController extends Controller
{
    public function index(){
        $categories = Category::all();
        return view('backend.category.index',compact('categories'));
    }

    public function create(){
        $parent = Category::select('id','c_name','parent_id')->get()->toArray();
        return view('backend.category.create',compact('parent'));
    }

    public function createPost(Request $request){
        $category                    = new Category();
        $category->c_name            = $request->name;
        $category->c_slug            = Str::slug($request->name);
        $category->c_title_seo       = $request->c_title_seo ? $request->c_title_seo : $request->name;
        $category->c_description_seo = $request->c_description_seo;
        $category->parent_id         = $request->sltParent;
        $category->save();

        return redirect()->route('admin.category.index')->with('success','Thêm mới danh mục thành công');
    }

    public function update($id){
        $parent = Category::select('id','c_name','parent_id')->get()->toArray();
        $category = Category::find($id);
        return view('backend.category.update',compact('parent','category'));
    }

    public function updatePost(Request $request,$id){
        $category                    = Category::find($id);
        $category->c_name            = $request->name;
        $category->c_slug            = Str::slug($request->name);
        $category->c_title_seo       = $request->c_title_seo ? $request->c_title_seo : $request->name;
        $category->c_description_seo = $request->c_description_seo;
        $category->parent_id         = $request->sltParent;
        $category->save();
        return redirect()->route('admin.category.index')->with('success','Cập nhật danh mục thành công');
    }

    public function action($action,$id){
        if($action)
        {
            $category = Category::find($id);

            switch ($action)
            {
                case 'delete':
                    $category->delete();
                    $msgCate = 'Xóa danh mục thành công';
                    break;
                case 'home':
                    $category->c_home = $category->c_home == 1 ? 0 : 1 ;
                    $category->save();
                    $msgCate = 'Cập nhật trang chủ danh mục thành công';
                    break;
                case 'active':
                    $category->c_active = $category->c_active == 1 ? 0 : 1 ;
                    $category->save();
                    $msgCate = 'Cập nhật trạng thái danh mục thành công';
                    break;
            }
        }
        return redirect()->back()->with('success',$msgCate);
    }
}
