<?php

namespace App\Http\Controllers\Backend;

use App\Models\Article;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;

class ArticleController extends Controller
{
    public function index(Request $request){
        $articles =  $articles = Article::with('category:id,c_name')->whereRaw(1);
        //tiem kiem bai viet
        if($request->name) $articles->where('a_name','like','%'.$request->name.'%');

        $articles = $articles->paginate(10);

        return view('backend.articles.index',compact('articles'));
    }

    public function create(){
        $categories = Category::all();
        return view('backend.articles.create',compact('categories'));
    }

    public function createPost(Request $request){
        $this->insertOrUpdate($request);
        return redirect()->route('admin.article.index')->with('success','Thêm mới bài viết thành công');
    }

    public function insertOrUpdate($request, $id=''){
        $article = new Article();
        if($id) $article = Article::find($id);

        $article->a_category_id = $request->a_category_id;
        $article->a_tag = $request->a_tag;
        $article->a_name = $request->a_name;
        $article->a_slug = Str::slug($request->a_name);
        $article->a_description = $request->a_description;
        $article->a_content = $request->a_content;
        $article->a_title_seo = $request->a_title_seo ? $request->a_title_seo :  $request->a_name;
        $article->a_description_seo = $request->a_description_seo ?  $request->a_description_seo : $request->a_description;

        if($request->hasFile('avatar'))
        {
            $file = upload_image('avatar');
            if(isset($file['name']))
            {
                $article->a_avatar = $file['name'];
            }
            //dd($file);
        }

        $article->save();
    }

    public function update($id){
        $categories = Category::all();
        $article = Article::find($id);
        return view('backend.articles.update',compact('categories','article'));
    }

    public function updatePost(Request $request,$id){
        $this->insertOrUpdate($request,$id);
        return redirect()->route('admin.article.index')->with('success','Cập nhật bài viết thành công');
    }
    public function action($action,$id){
        if($action)
        {
            $article = Article::find($id);

            switch ($action)
            {
                case 'delete':
                    $article->delete();
                    $msgArticle = 'Xóa Bài viết thành công';
                    break;

                case 'active':
                    $article->a_active = $article->a_active ? 0 : 1;
                    $article->save();
                    $msgArticle = 'Cập nhật trạng thái Bài viết thành công';
                    break;
                case 'hot':
                    $article->a_hot = $article->a_hot ? 0 : 1;
                    $msgArticle = 'Cập nhật nổi bật Bài viết thành công';
                    $article->save();
                    break;
            }
        }
        return redirect()->back()->with('success',$msgArticle);
    }
}
