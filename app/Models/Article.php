<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

class Article extends Model
{
    protected $table = 'articles';
    protected $guarded = [''];

    public function category(){
        return $this->belongsTo(Category::class,'a_category_id');
    }

    const HOT = 1;

    protected $status = [
        1 => [
            'name' => 'Public',
            'class'=> 'label-danger'
        ],
        0 =>[
            'name' =>'Private',
            'class'=>'label-default'
        ]
    ];

    protected $hot = [
        1 => [
            'name' => 'Public',
            'class'=> 'label-success'
        ],
        0 =>[
            'name' =>'Private',
            'class'=>'label-default'
        ]
    ];

    public function getStatus(){
        return Arr::get($this->status,$this->a_active, '[N\A]');
    }

    public function getHot(){
        return Arr::get($this->hot,$this->a_hot, '[N\A]');
    }
}
